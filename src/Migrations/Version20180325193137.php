<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180325193137 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entreprise MODIFY idEntreprise INT NOT NULL');
        $this->addSql('ALTER TABLE entreprise DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE entreprise ADD id_entreprise INT NOT NULL, ADD nom_entreprise LONGTEXT DEFAULT NULL, ADD ville_entreprise LONGTEXT DEFAULT NULL, ADD adresse_entreprise LONGTEXT DEFAULT NULL, ADD mail_entreprise LONGTEXT DEFAULT NULL, ADD tel_entreprise VARCHAR(255) DEFAULT NULL, ADD activite_entreprise LONGTEXT DEFAULT NULL, DROP idEntreprise, DROP nomEntreprise, DROP villeEntreprise, DROP adresseEntreprise, DROP mailEntreprise, DROP telEntreprise, DROP activiteEntreprise, CHANGE active active TINYINT(1) DEFAULT NULL, CHANGE cpentreprise cp_entreprise INT DEFAULT NULL');
        $this->addSql('ALTER TABLE entreprise ADD PRIMARY KEY (id_entreprise)');
        $this->addSql('ALTER TABLE stage MODIFY idStage INT NOT NULL');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY fk_Stage_Tuteur1');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY fk_Stage_UserEleve');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY fk_Stage_UserProf1');
        $this->addSql('DROP INDEX fk_Stage_UserEleve_idx ON stage');
        $this->addSql('DROP INDEX fk_Stage_UserProf1_idx ON stage');
        $this->addSql('DROP INDEX fk_Stage_Tuteur1_idx ON stage');
        $this->addSql('ALTER TABLE stage DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE stage ADD id_user_eleve INT NOT NULL, ADD id_user_prof INT NOT NULL, ADD id_tuteur INT NOT NULL, DROP idUserEleve, DROP idUserProf, DROP idTuteur, CHANGE idstage id INT AUTO_INCREMENT NOT NULL, CHANGE datestage date_stage DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE stage ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE tuteur MODIFY idTuteur INT NOT NULL');
        $this->addSql('ALTER TABLE tuteur DROP FOREIGN KEY fk_Tuteur_Entreprise1');
        $this->addSql('DROP INDEX fk_Tuteur_Entreprise1_idx ON tuteur');
        $this->addSql('ALTER TABLE tuteur DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tuteur ADD nom_tuteur LONGTEXT DEFAULT NULL, ADD prenom_tuteur LONGTEXT DEFAULT NULL, ADD mail_tuteur LONGTEXT DEFAULT NULL, ADD tel_tuteur VARCHAR(255) DEFAULT NULL, DROP nomTuteur, DROP prenomTuteur, DROP mailTuteur, DROP telTuteur, CHANGE idtuteur id INT AUTO_INCREMENT NOT NULL, CHANGE identreprise id_entreprise INT NOT NULL');
        $this->addSql('ALTER TABLE tuteur ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE usereleve MODIFY idUserEleve INT NOT NULL');
        $this->addSql('ALTER TABLE usereleve DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE usereleve ADD nom_eleve LONGTEXT DEFAULT NULL, ADD prenom_eleve LONGTEXT DEFAULT NULL, ADD annee_scolaire LONGTEXT DEFAULT NULL, DROP nomEleve, DROP prenomEleve, DROP anneeScolaire, CHANGE role role VARCHAR(255) DEFAULT NULL, CHANGE present present TINYINT(1) DEFAULT NULL, CHANGE idusereleve id INT AUTO_INCREMENT NOT NULL, CHANGE classeeleve classe_eleve INT DEFAULT NULL');
        $this->addSql('ALTER TABLE usereleve ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE userprof MODIFY idUserProf INT NOT NULL');
        $this->addSql('ALTER TABLE userprof DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE userprof ADD nom_prof LONGTEXT DEFAULT NULL, ADD prenom_prof LONGTEXT DEFAULT NULL, DROP nomProf, DROP prenomProf, CHANGE role role LONGTEXT DEFAULT NULL, CHANGE present present INT DEFAULT NULL, CHANGE iduserprof id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE userprof ADD PRIMARY KEY (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entreprise DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE entreprise ADD idEntreprise INT AUTO_INCREMENT NOT NULL, ADD nomEntreprise TEXT DEFAULT NULL COLLATE utf8_general_ci, ADD villeEntreprise TEXT DEFAULT NULL COLLATE utf8_general_ci, ADD adresseEntreprise TEXT DEFAULT NULL COLLATE utf8_general_ci, ADD mailEntreprise TEXT DEFAULT NULL COLLATE utf8_general_ci, ADD telEntreprise VARCHAR(20) DEFAULT NULL COLLATE utf8_general_ci, ADD activiteEntreprise TEXT DEFAULT NULL COLLATE utf8_general_ci, DROP id_entreprise, DROP nom_entreprise, DROP ville_entreprise, DROP adresse_entreprise, DROP mail_entreprise, DROP tel_entreprise, DROP activite_entreprise, CHANGE active active TINYINT(1) DEFAULT \'1\', CHANGE cp_entreprise cpEntreprise INT DEFAULT NULL');
        $this->addSql('ALTER TABLE entreprise ADD PRIMARY KEY (idEntreprise)');
        $this->addSql('ALTER TABLE stage MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE stage DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE stage ADD idUserEleve INT NOT NULL, ADD idUserProf INT NOT NULL, ADD idTuteur INT NOT NULL, DROP id_user_eleve, DROP id_user_prof, DROP id_tuteur, CHANGE id idStage INT AUTO_INCREMENT NOT NULL, CHANGE date_stage dateStage DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT fk_Stage_Tuteur1 FOREIGN KEY (idTuteur) REFERENCES tuteur (idTuteur) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT fk_Stage_UserEleve FOREIGN KEY (idUserEleve) REFERENCES usereleve (idUserEleve) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT fk_Stage_UserProf1 FOREIGN KEY (idUserProf) REFERENCES userprof (idUserProf) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX fk_Stage_UserEleve_idx ON stage (idUserEleve)');
        $this->addSql('CREATE INDEX fk_Stage_UserProf1_idx ON stage (idUserProf)');
        $this->addSql('CREATE INDEX fk_Stage_Tuteur1_idx ON stage (idTuteur)');
        $this->addSql('ALTER TABLE stage ADD PRIMARY KEY (idStage, idUserEleve, idUserProf, idTuteur)');
        $this->addSql('ALTER TABLE tuteur MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE tuteur DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tuteur ADD nomTuteur TEXT DEFAULT NULL COLLATE utf8_general_ci, ADD prenomTuteur TEXT DEFAULT NULL COLLATE utf8_general_ci, ADD mailTuteur TEXT DEFAULT NULL COLLATE utf8_general_ci, ADD telTuteur VARCHAR(20) DEFAULT NULL COLLATE utf8_general_ci, DROP nom_tuteur, DROP prenom_tuteur, DROP mail_tuteur, DROP tel_tuteur, CHANGE id idTuteur INT AUTO_INCREMENT NOT NULL, CHANGE id_entreprise idEntreprise INT NOT NULL');
        $this->addSql('ALTER TABLE tuteur ADD CONSTRAINT fk_Tuteur_Entreprise1 FOREIGN KEY (idEntreprise) REFERENCES entreprise (idEntreprise) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX fk_Tuteur_Entreprise1_idx ON tuteur (idEntreprise)');
        $this->addSql('ALTER TABLE tuteur ADD PRIMARY KEY (idTuteur, idEntreprise)');
        $this->addSql('ALTER TABLE usereleve MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE usereleve DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE usereleve ADD nomEleve TEXT DEFAULT NULL COLLATE utf8_general_ci, ADD prenomEleve TEXT DEFAULT NULL COLLATE utf8_general_ci, ADD anneeScolaire TEXT DEFAULT NULL COLLATE utf8_general_ci, DROP nom_eleve, DROP prenom_eleve, DROP annee_scolaire, CHANGE role role VARCHAR(200) DEFAULT \'eleve\' COLLATE utf8_general_ci, CHANGE present present TINYINT(1) DEFAULT \'1\', CHANGE id idUserEleve INT AUTO_INCREMENT NOT NULL, CHANGE classe_eleve classeEleve INT DEFAULT NULL');
        $this->addSql('ALTER TABLE usereleve ADD PRIMARY KEY (idUserEleve)');
        $this->addSql('ALTER TABLE userprof MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE userprof DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE userprof ADD nomProf TEXT DEFAULT NULL COLLATE utf8_general_ci, ADD prenomProf TEXT DEFAULT NULL COLLATE utf8_general_ci, DROP nom_prof, DROP prenom_prof, CHANGE role role VARCHAR(200) DEFAULT \'prof\' COLLATE utf8_general_ci, CHANGE present present TINYINT(1) DEFAULT \'1\', CHANGE id idUserProf INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE userprof ADD PRIMARY KEY (idUserProf)');
    }
}
