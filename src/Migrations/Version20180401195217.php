<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180401195217 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entreprise CHANGE telEntreprise telEntreprise VARCHAR(255) DEFAULT NULL, CHANGE active active TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE stage MODIFY idStage INT NOT NULL');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY fk_Stage_Tuteur1');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY fk_Stage_UserEleve');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY fk_Stage_UserProf1');
        $this->addSql('DROP INDEX fk_Stage_UserEleve_idx ON stage');
        $this->addSql('DROP INDEX fk_Stage_UserProf1_idx ON stage');
        $this->addSql('DROP INDEX fk_Stage_Tuteur1_idx ON stage');
        $this->addSql('ALTER TABLE stage DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE stage ADD PRIMARY KEY (idStage)');
        $this->addSql('ALTER TABLE tuteur MODIFY idTuteur INT NOT NULL');
        $this->addSql('ALTER TABLE tuteur DROP FOREIGN KEY fk_Tuteur_Entreprise1');
        $this->addSql('DROP INDEX fk_Tuteur_Entreprise1_idx ON tuteur');
        $this->addSql('ALTER TABLE tuteur DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tuteur CHANGE telTuteur telTuteur VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE tuteur ADD PRIMARY KEY (idTuteur)');
        $this->addSql('ALTER TABLE usereleve CHANGE role role VARCHAR(255) DEFAULT NULL, CHANGE present present TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE userprof CHANGE role role LONGTEXT DEFAULT NULL, CHANGE present present INT DEFAULT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entreprise CHANGE telEntreprise telEntreprise VARCHAR(20) DEFAULT NULL COLLATE utf8_general_ci, CHANGE active active TINYINT(1) DEFAULT \'1\'');
        $this->addSql('ALTER TABLE stage MODIFY idStage INT NOT NULL');
        $this->addSql('ALTER TABLE stage DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT fk_Stage_Tuteur1 FOREIGN KEY (idTuteur) REFERENCES tuteur (idTuteur) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT fk_Stage_UserEleve FOREIGN KEY (idUserEleve) REFERENCES usereleve (idUserEleve) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT fk_Stage_UserProf1 FOREIGN KEY (idUserProf) REFERENCES userprof (idUserProf) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX fk_Stage_UserEleve_idx ON stage (idUserEleve)');
        $this->addSql('CREATE INDEX fk_Stage_UserProf1_idx ON stage (idUserProf)');
        $this->addSql('CREATE INDEX fk_Stage_Tuteur1_idx ON stage (idTuteur)');
        $this->addSql('ALTER TABLE stage ADD PRIMARY KEY (idStage, idUserEleve, idUserProf, idTuteur)');
        $this->addSql('ALTER TABLE tuteur MODIFY idTuteur INT NOT NULL');
        $this->addSql('ALTER TABLE tuteur DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE tuteur CHANGE telTuteur telTuteur VARCHAR(20) DEFAULT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE tuteur ADD CONSTRAINT fk_Tuteur_Entreprise1 FOREIGN KEY (idEntreprise) REFERENCES entreprise (idEntreprise) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX fk_Tuteur_Entreprise1_idx ON tuteur (idEntreprise)');
        $this->addSql('ALTER TABLE tuteur ADD PRIMARY KEY (idTuteur, idEntreprise)');
        $this->addSql('ALTER TABLE usereleve CHANGE role role VARCHAR(200) DEFAULT \'eleve\' COLLATE utf8_general_ci, CHANGE present present TINYINT(1) DEFAULT \'1\'');
        $this->addSql('ALTER TABLE userprof CHANGE role role VARCHAR(200) DEFAULT \'prof\' COLLATE utf8_general_ci, CHANGE present present TINYINT(1) DEFAULT \'1\'');
    }
}
