<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserprofRepository")
 */
class Userprof
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\OneToMany(targetEntity="App\Entity\Userprof", mappedBy="stage")
     * @ORM\Column(name="idUserProf", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="nomProf", type="text", nullable=true)
     */
    private $nomProf;

    /**
     * @ORM\Column(name="prenomProf", type="text", nullable=true)
     */
    private $prenomProf;

    /**
     * @ORM\Column(name="login", type="text", nullable=true)
     */
    private $login;

    /**
     * @ORM\Column(name="password", type="text", nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(name="role", type="text", nullable=true)
     */
    private $role;

    /**
     * @ORM\Column(name="present", type="integer", nullable=true)
     */
    private $present;

    public function getId()
    {
        return $this->id;
    }

    public function getNomProf(): ?string
    {
        return $this->nomProf;
    }

    public function setNomProf(?string $nomProf): self
    {
        $this->nomProf = $nomProf;

        return $this;
    }

    public function getPrenomProf(): ?string
    {
        return $this->prenomProf;
    }

    public function setPrenomProf(?string $prenomProf): self
    {
        $this->prenomProf = $prenomProf;

        return $this;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(?string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(?string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getPresent(): ?bool
    {
        return $this->present;
    }

    public function setPresent(?bool $present): self
    {
        $this->present = $present;

        return $this;
    }
}
