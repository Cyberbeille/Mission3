<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Entreprise as Entreprise;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TuteurRepository")
 */
class Tuteur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="idTuteur", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="nomTuteur", type="text", nullable=true)
     */
    private $nomTuteur;

    /**
     * @ORM\Column(name="prenomTuteur", type="text", nullable=true)
     */
    private $prenomTuteur;

    /**
     * @ORM\Column(name="mailTuteur", type="text", nullable=true)
     */
    private $mailTuteur;

    /**
     * @ORM\Column(name="telTuteur", type="string", length=255, nullable=true)
     */
    private $telTuteur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entreprise")
     * @ORM\JoinColumn(name="idEntreprise", referencedColumnName="idEntreprise")
     */
    private $entreprise;

    public function getId()
    {
        return $this->id;
    }

    public function getNomTuteur(): ?string
    {
        return $this->nomTuteur;
    }

    public function setNomTuteur(?string $nomTuteur): self
    {
        $this->nomTuteur = $nomTuteur;

        return $this;
    }

    public function getPrenomTuteur(): ?string
    {
        return $this->prenomTuteur;
    }

    public function setPrenomTuteur(string $prenomTuteur): self
    {
        $this->prenomTuteur = $prenomTuteur;

        return $this;
    }

    public function getMailTuteur(): ?string
    {
        return $this->mailTuteur;
    }

    public function setMailTuteur(?string $mailTuteur): self
    {
        $this->mailTuteur = $mailTuteur;

        return $this;
    }

    public function getTelTuteur(): ?string
    {
        return $this->telTuteur;
    }

    public function setTelTuteur(?string $telTuteur): self
    {
        $this->telTuteur = $telTuteur;

        return $this;
    }

    public function getEntreprise() : ?Entreprise
    {
        return $this->entreprise;
    }

    public function setEntreprise(?Entreprise $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }
}

