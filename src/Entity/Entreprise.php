<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @ORM\Table(name="entreprise")
 * @ORM\Entity(repositoryClass="App\Repository\EntrepriseRepository")
 */
class Entreprise
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\OneToMany(targetEntity="App\Entity\Entreprise", mappedBy="tuteur")
     * @ORM\JoinColumn(nullable=true)
     * @ORM\Column(name="idEntreprise",type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="nomEntreprise",type="text", nullable=true)
     */
    private $nomEntreprise;

    /**
     * @ORM\Column(name="villeEntreprise",type="text", nullable=true)
     */
    private $villeEntreprise;

    /**
     * @ORM\Column(name="cpEntreprise",type="integer", nullable=true)
     */
    private $cpEntreprise;

    /**
     * @ORM\Column(name="adresseEntreprise",type="text", nullable=true)
     */
    private $adresseEntreprise;

    /**
     * @ORM\Column(name="mailEntreprise",type="text", nullable=true)
     */
    private $mailEntreprise;

    /**
     * @ORM\Column(name="telEntreprise",type="string", length=255, nullable=true)
     */
    private $telEntreprise;

    /**
     * @ORM\Column(name="activiteEntreprise",type="text", nullable=true)
     */
    private $activiteEntreprise;

    /**
     * @ORM\Column(name="active",type="boolean", nullable=true)
     */
    private $active;

    public function getId()
    {
        return $this->id;
    }

    public function getNomEntreprise(): ?string
    {
        return $this->nomEntreprise;
    }

    public function setNomEntreprise(?string $nomEntreprise): self
    {
        $this->nomEntreprise = $nomEntreprise;

        return $this;
    }

    public function getVilleEntreprise(): ?string
    {
        return $this->villeEntreprise;
    }

    public function setVilleEntreprise(?string $villeEntreprise): self
    {
        $this->villeEntreprise = $villeEntreprise;

        return $this;
    }

    public function getCpEntreprise(): ?int
    {
        return $this->cpEntreprise;
    }

    public function setCpEntreprise(?int $cpEntreprise): self
    {
        $this->cpEntreprise = $cpEntreprise;

        return $this;
    }

    public function getAdresseEntreprise(): ?string
    {
        return $this->adresseEntreprise;
    }

    public function setAdresseEntreprise(?string $adresseEntreprise): self
    {
        $this->adresseEntreprise = $adresseEntreprise;

        return $this;
    }

    public function getMailEntreprise(): ?string
    {
        return $this->mailEntreprise;
    }

    public function setMailEntreprise(?string $mailEntreprise): self
    {
        $this->mailEntreprise = $mailEntreprise;

        return $this;
    }

    public function getTelEntreprise(): ?string
    {
        return $this->telEntreprise;
    }

    public function setTelEntreprise(?string $telEntreprise): self
    {
        $this->telEntreprise = $telEntreprise;

        return $this;
    }

    public function getActiviteEntreprise(): ?string
    {
        return $this->activiteEntreprise;
    }

    public function setActiviteEntreprise(?string $activiteEntreprise): self
    {
        $this->activiteEntreprise = $activiteEntreprise;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
