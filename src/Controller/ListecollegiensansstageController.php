<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ListecollegiensansstageController extends Controller
{
    /**
     * @Route("/listecollegiensansstage", name="listecollegiensansstage")
     */
    public function index()
    {
        return $this->render('listecollegiensansstage/listecollegiensansstage.html.twig', [
            'controller_name' => 'ListecollegiensansstageController',
        ]);
    }
}
