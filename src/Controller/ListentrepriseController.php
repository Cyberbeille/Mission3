<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ListentrepriseController extends Controller
{
    /**
     * @Route("/listentreprise", name="listentreprise")
     */
    public function index()
    {
        return $this->render('listentreprise/listentreprise.html.twig', [
            'controller_name' => 'ListentrepriseController',
        ]);
    }
}
