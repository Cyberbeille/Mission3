<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AideprofController extends Controller
{
    /**
     * @Route("/aideprof", name="aideprof")
     */
    public function index()
    {
        return $this->render('aideprof/aideprof.html.twig', [
            'controller_name' => 'AideprofController',
        ]);
    }
}
