<?php

namespace App\Controller;

use App\Entity\Usereleve;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;

class ListeleveController extends Controller
{
    /**
     * @Route("/listeleve", name="listeleve")
     */
    public function index()
    {
        $eleve = $this->getDoctrine()
            ->getRepository(Usereleve::class)
            ->findAll();
        return $this->render('listeleve/listeleve.html.twig', compact('eleve'));
    }

    /**
     * @Route("/listeleve/ajout", name="ajouteleve")
     */
    public function ajoutEleve(Request $request)
    {
        $item = new Usereleve();

        $item->setNomEleve('');
        $item->setPrenomEleve('');
        $item->setClasseEleve('0');
        $item->setAnneeScolaire('2002-2003');
        $item->setLogin('');
        $item->setPassword('');
        $item->setRole('eleve');
        $item->setPresent('');

        $form = $this->createFormBuilder($item)
            ->add('nomEleve', TextType::class, array(
                'label' => 'Nom'
            ))
            ->add('prenomEleve', TextType::class, array(
                'label' => 'Prenom'
            ))
            ->add('classeEleve', IntegerType::class, array(
                'label' => 'Classe'
            ))
            ->add('anneeScolaire', TextType::class, array(
                'label' => 'Année Scolaire',
            ))
            ->add('login', TextType::class, array(
                'label' => 'Identifiant'
            ))
            ->add('password', PasswordType::class, array(
                'label' => 'Mot de passe'
            ))
            ->add('role', HiddenType::class)
            ->add('present',  ChoiceType::class, array(
                'choices'  => array(
                    'Oui' => true,
                    'Non' => false,
                ),
                'label' => 'Présent'
            ))
            ->getForm();

        // Par défaut, le formulaire renvoie une demande POST au même contrôleur qui la restitue.
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $item = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($item);
                $em->flush();
                return $this->redirectToRoute('listeleve');
            }
        }

        return $this->render('listeleve/ajout.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/listeleve/supprimer/{id}", name="supprimereleve")
     */
    public function supprimerEleve($id)
    {
        $item = $this->getDoctrine()
            ->getRepository(Usereleve::class)
            ->find($id);

        if (!$item) {
            throw $this->createNotFoundException(
                "Aucun éleve n'a été trouvée via l'id " . $id
            );
        }else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($item);
            $em->flush();
        }

        // Par défaut on retourne à la liste
        return $this->redirectToRoute('listeleve');
    }

    /**
     * @Route("/listeleve/modifier/{id}", name="modifiereleve")
     */
    public function modifierEleve(Request $request, $id)
    {
        $item = $this->getDoctrine()
        ->getRepository(Usereleve::class)
        ->find($id);
        if (!$item) {
            throw $this->createNotFoundException(
                "Aucun éleve n'a été trouvée via l'id " . $id
            );
        } else {
            $form = $this->createFormBuilder($item)
                ->add('nomEleve', TextType::class)
                ->add('prenomEleve', TextType::class)
                ->add('classeEleve', IntegerType::class)
                ->add('anneeScolaire', TextType::class)
                ->add('login', TextType::class)
                ->add('password', PasswordType::class)
                ->add('present',  ChoiceType::class, array(
                    'choices'  => array(
                        'Oui' => true,
                        'Non' => false,
                    ),
                ))
                ->getForm();
        }
        // Par défaut, demande POST au même contrôleur qui la restitue.
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $item = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($item);
                $em->flush();
                return $this->redirectToRoute('listeleve');
            }
        }
        return $this->render('listeleve/ajout.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}