<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProfeleveController extends Controller
{
    /**
     * @Route("/profeleve", name="profeleve")
     */
    public function index()
    {
        return $this->render('profeleve/profeleve.html.twig', [
            'controller_name' => 'ProfeleveController',
        ]);
    }
}
