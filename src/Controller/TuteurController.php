<?php

namespace App\Controller;

use App\Entity\Entreprise;
use App\Entity\Tuteur;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
class TuteurController extends Controller
{
    /**
     * @Route("/Tuteur", name="Tuteur")
     */
    public function index()
    {
        $tuteurs = $this->getDoctrine()
            ->getRepository(Tuteur::class)
            ->findAll();
        return $this->render('Tuteur/Tuteur.html.twig', compact('tuteurs'));
    }


    /**
     * @Route("/tuteur/ajout", name="ajouttuteur")
     */
    public function ajoutTuteur(Request $request)
    {
        $item = new Tuteur();

        $item->setnomTuteur('');
        $item->setprenomTuteur('');
        $item->setmailTuteur('');
        $item->settelTuteur('');
        $item->setEntreprise('');

        $form = $this->createFormBuilder($item)
            ->add('nomTuteur', TextType::class, array(
                'label' => 'Nom'
            ))
            ->add('prenomTuteur', TextType::class, array(
                'label' => 'Prenom'
            ))
            ->add('mailTuteur', TextType::class, array(
                'label' => 'Email'
            ))
            ->add('telTuteur', TextType::class, array(
                'label' => 'Téléphone',
            ))
            ->add('Entreprise', TextType::class, array(
                'class' => Entreprise::class,
                'label' => 'Nom Entreprise'
            ))


            ->getForm();

        // Par défaut, le formulaire renvoie une demande POST au même contrôleur qui la restitue.
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $item = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($item);
                $em->flush();
                return $this->redirectToRoute('Tuteur');
            }
        }

        return $this->render('Tuteur/ajout.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/tuteur/supprimer/{id}", name="supprimertuteur")
     */
    public function supprimerTuteur($id)
    {
        $item = $this->getDoctrine()
            ->getRepository(Tuteur::class)
            ->find($id);

        if (!$item) {
            throw $this->createNotFoundException(
                "Aucun tuteur n'a été trouvée via l'id " . $id
            );
        }else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($item);
            $em->flush();
        }

        // Par défaut on retourne à la liste
        return $this->redirectToRoute('Tuteur');
    }

    /**
     * @Route("/tuteur/modifier/{id}", name="modifiertuteur")
     */
    public function modifierTuteur(Request $request, $id)
    {
        $item = $this->getDoctrine()
            ->getRepository(Tuteur::class)
            ->find($id);
        if (!$item) {
            throw $this->createNotFoundException(
                "Aucun éleve n'a été trouvée via l'id " . $id
            );
        } else {
            $form = $this->createFormBuilder($item)
                ->add('nomTuteur', TextType::class, array(
                    'label' => 'Nom'
                ))
                ->add('prenomTuteur', TextType::class, array(
                    'label' => 'Prenom'
                ))
                ->add('mailTuteur', EmailType::class, array(
                    'label' => 'Email'
                ))
                ->add('telTuteur', TextType::class, array(
                    'label' => 'Téléphone',
                ))
                ->add('Entreprise', TextType::class, array(
                    'label' => 'Nom Entreprise'
                ))
                ->getForm();
        }
        // Par défaut, demande POST au même contrôleur qui la restitue.
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $item = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($item);
                $em->flush();
                return $this->redirectToRoute('Tuteur');
            }
        }
        return $this->render('Tuteur/ajout.html.twig', array(
            'form' => $form->createView(),
        ));
    }


}



