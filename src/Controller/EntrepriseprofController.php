<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EntrepriseprofController extends Controller
{
    /**
     * @Route("/entrepriseprof", name="entrepriseprof")
     */
    public function index()
    {
        return $this->render('entrepriseprof/entrepriseprof.html.twig', [
            'controller_name' => 'EntrepriseprofController',
        ]);
    }
}
