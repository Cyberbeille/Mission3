<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ListecollegienavecstageController extends Controller
{
    /**
     * @Route("/listecollegienavecstage", name="listecollegienavecstage")
     */
    public function index()
    {
        return $this->render('listecollegienavecstage/listecollegienavecstage.html.twig', [
            'controller_name' => 'ListecollegienavecstageController',
        ]);
    }
}
