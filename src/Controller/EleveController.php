<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EleveController extends Controller
{
    /**
     * @Route("/eleve", name="eleve")
     */
    public function index()
    {
        return $this->render('eleve/eleveaccueil.html.twig', [
            'controller_name' => 'EleveController',
        ]);
    }
}
