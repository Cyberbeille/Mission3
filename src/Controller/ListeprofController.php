<?php

namespace App\Controller;

use App\Entity\Userprof;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ListeprofController extends Controller
{
    /**
     * @Route("/listeprof", name="listeprof")
     */
    public function index()
    {
        $professeur = $this->getDoctrine()
            ->getRepository(Userprof::class)
            ->findAll();
        return $this->render('listeprof/listeprof.html.twig', compact('professeur'));
    }
}
